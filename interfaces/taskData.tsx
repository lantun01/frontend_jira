import { UserData } from "./userData";
import { projectData } from "./projectData";
export interface TaskData {
    id: number;
    name: string;
    description: string;
    creator: UserData;
    responsible: UserData;
    startDate: string;
    endDate: string;
    taskStatus: number;
    project: projectData;
  }
  