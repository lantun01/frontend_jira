export interface projectData{
    id: number;
    name: string;
    description: string;
    startDate: Date;
}