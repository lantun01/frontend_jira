import axios, { AxiosError } from 'axios';
import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import ConfirmationModal from '../../components/modalConfirmacion';
import { UserStore } from '../../store/UserStore';
import { Dropdown } from 'react-native-element-dropdown';
import { RootStackParamList } from '../../types/rootsStackParamList';
import { StackNavigationProp } from '@react-navigation/stack';

type DeleteTaskProps = {
  navigation: StackNavigationProp<RootStackParamList, 'main_task'>;
};

const DeleteTask: React.FC<DeleteTaskProps> = ({ navigation }) => {
  const [idMain, setidMain] = useState<number>(0);
  const [name, setName] = useState('');
  const [modalVisible, setModalVisible] = useState(false);//confirmacion
  const {id} =UserStore();
  const [data,setdata] =useState([])///data de tareas
  const [isFocus, setIsFocus] = useState(false);//de dropdown


  const Update =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/tasks'
      const body ={
        "id": idMain,
        "visible": false
      }
      const response =await axios.put(url,body)
      navigation.navigate('mainValid')

    }catch (error) {
      const axiosError = error as AxiosError | undefined; 
      if (axiosError) {
        console.error('Error al realizar la solicitud:', axiosError.response?.data?.message);
        Alert.alert(axiosError.response?.data?.message || 'Hubo un error en la solicitud.');
      } else {
        console.error('Error al realizar la solicitud:', error);
        Alert.alert('Hubo un error en la solicitud.');
      }
    }
    handleCancelChanges();
    setModalVisible(false);
    Alert.alert('se ha eliminado una tarea')
  };
  //////////////////////////////////////////////////////////////////////////////////////////////

  //no guardar cambios
  const handleCancelChanges = () => {
    setName('');
    setModalVisible(false);
  };

  //esto es para cargar los datos sin depender de un boton
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/tasks');
        setdata(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  //

  return (
      <View style={styles.inner} >

      <View style={styles.container}>
      <Text style={styles.title}>Eliminar una tarea</Text>

      <Dropdown
        style={[styles.input, isFocus && { borderColor: 'blue' }]}
        data={data}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={!isFocus ? 'Seleccione la tarea' : '...'}
        onChange={item => {
          setidMain(item.id);///aqui se selecciona el campo para cambiar
          setIsFocus(false);
        }}
      />

      <TouchableOpacity style={styles.button} onPress={() =>setModalVisible(true)} >
        <Text style={styles.buttonText}>Eliminar</Text>
      </TouchableOpacity>

      </View>
     

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={Update}
        onCancel={handleCancelChanges}
      />
       </View>
  
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    backgroundColor: 'orange',//color 
    alignItems: 'center',//alinea al centro
    
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 0,
    textDecorationLine: 'underline'
  },
  input: {
    borderWidth:10,
    borderColor:'brown',
    paddingStart:30,
    width: '80%',
    height:70,
    padding: 10,
    marginTop:30,//separacion entre las columnas
    backgroundColor: '#58f858',
  },
  button: {
    backgroundColor: 'blue',
    marginTop:20,
    padding: 10,
    borderRadius: 50,
    width: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  inner: {
    flex: 1,
    justifyContent: 'center',
  },
  text_button:{
    //color: 'white',
    fontSize: 18,
    textAlign: 'center',
    justifyContent:'center',
    //backgroundColor:'blue',
    marginEnd:28,
    marginTop:10
  },
  principal:{
    flex:1,
    backgroundColor:'green',
  }
 
 
});

export default DeleteTask;