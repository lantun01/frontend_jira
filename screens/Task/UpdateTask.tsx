import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert,TextInputComponent} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { DateTimePickerAndroid } from '@react-native-community/datetimepicker';
import ConfirmationModal from '../../components/modalConfirmacion';
import { UserStore } from '../../store/UserStore';
import { format } from 'date-fns';
import { Dropdown } from 'react-native-element-dropdown';


const UpdateTask = ({navigation}) => {
  const [idMain, setidMain] = useState<number>(0);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [responsibleName, setresponsibleName] = useState('');
  const [initDate, setinitDate] = useState(new Date());
  const [endDate, setendDate] = useState(new Date());
  const [proyectName, setproyectName] = useState('');
  const [date, setDate] = useState(new Date());
  const [modalVisible, setModalVisible] = useState(false);//confirmacion
  const {id} =UserStore();
  const [data,setdata] =useState([])///data de tareas
  const [isFocus, setIsFocus] = useState(true);//de dropdown


//////////////////////////calendario
const onChange = (event, selectedDate, dateType) => {
  const currentDate = selectedDate || date;
  if (dateType === 'start') {
    setinitDate(currentDate);
  } else {
    setendDate(currentDate);
  }
};
//////////////////////////////////////////////////////////////
  const showMode = (currentMode, dateType) => {
  DateTimePickerAndroid.open({
    value: dateType === 'start' ? initDate : endDate,
    onChange: (event, selectedDate) => onChange(event, selectedDate, dateType),
    mode: currentMode,
    is24Hour: true,
  });
  };
  //////////////////////////////////////////////////////////////
  const showDatepicker = (dateType) => {
    showMode('date', dateType);
  };
  ///////////////////////////////////////////////////////
  
  const fecha_inicial = () => {
    showDatepicker('start');
  };
  const fecha_termino = () => {
    showDatepicker('end');
  };
 
  /////////////////////////////////////////////////////////////////////////////
  const Update =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/tasks'
      const body ={
        "id": idMain,
        "name": name,
        "description": description,
        "startDate": format(initDate, 'yyyy-MM-dd'),
        "endDate": format(endDate, 'yyyy-MM-dd'),
      }
      const response =await axios.put(url,body)
      Alert.alert('se ha actualizado una tarea')
      navigation.navigate('mainValid')

    }catch(error){
      console.error('Error al realizar la solicitud:',error.response.data.message.toString())
      Alert.alert(error.response.data.message.toString())
    }
    handleCancelChanges();
    setModalVisible(false);
  };
  //////////////////////////////////////////////////////////////////////////////////////////////

  //no guardar cambios
  const handleCancelChanges = () => {
    setName('');
    setDescription('');
    setresponsibleName('');
    setproyectName('');
    setinitDate(new Date());
    setendDate(new Date())
    setModalVisible(false);
  };

  //esto es para cargar los datos sin depender de un boton
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/tasks');
        setdata(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  //

  return (
    <KeyboardAwareScrollView  >
      
      <View style={styles.inner} >
      <Text style={styles.title}>Actualizar una tarea</Text>

      <View style={styles.container}>



      <Dropdown
        style={[styles.input, isFocus && { borderColor: 'blue' }]}
        data={data}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={!isFocus ? 'Seleccione la tarea a actualizar' : '...'}
        onChange={item => {
          setidMain(parseInt(item.id));///aqui se selecciona el campo para cambiar
          setIsFocus(false);
        }}
      />

      <TextInput
        placeholder="Nombre de la tarea"
        style={styles.input}
        onChangeText={text => setName(text)}
      />

      <TextInput
        placeholder="descripcion"
        style={styles.input}
        onChangeText={text => setDescription(text)}
      />

      <TouchableOpacity style={styles.input} onPress={fecha_inicial} >
        <Text style={styles.text_button}>Inicio = {initDate.toLocaleDateString()}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.input} onPress={fecha_termino} >
        <Text style={styles.text_button}>Termino = {endDate.toLocaleDateString()}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() =>setModalVisible(true)} >
        <Text style={styles.buttonText}>Actualizar</Text>
      </TouchableOpacity>

      </View>
      </View>

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={Update}
        onCancel={handleCancelChanges}
      />
      </KeyboardAwareScrollView>      
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    backgroundColor: 'orange',//color 
    alignItems: 'center',//alinea al centro
    justifyContent: 'center',
    paddingBottom:30
  },
  title: {
    fontSize: 24,
    marginBottom: 0,
    textDecorationLine: 'underline'
  },
  input: {
    borderWidth:1,
    borderColor:'blue',
    paddingStart:30,
    width: '80%',
    height:70,
    padding: 10,
    marginTop:30,//separacion entre las columnas
    borderRadius: 50,
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: 'blue',
    marginTop:20,
    padding: 10,
    borderRadius: 50,
    width: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  inner: {
    padding: 24,
    paddingBottom:130,
    flex: 1,
    justifyContent: 'center',
    backgroundColor:'orange'
  },
  text_button:{
    //color: 'white',
    fontSize: 18,
    textAlign: 'center',
    justifyContent:'center',
    //backgroundColor:'blue',
    marginEnd:28,
    marginTop:10
  },
 
 
});

export default UpdateTask;