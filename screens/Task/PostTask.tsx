import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert,TextInputComponent} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { DateTimePickerAndroid } from '@react-native-community/datetimepicker';
import ConfirmationModal from '../../components/modalConfirmacion';
import { UserStore } from '../../store/UserStore';
import { format } from 'date-fns';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';


const PostTask = ({navigation}) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [responsibleName, setresponsibleName] = useState('');
  const [initDate, setinitDate] = useState(new Date());
  const [endDate, setendDate] = useState(new Date());
  const [proyectName, setproyectName] = useState('');
  const [date, setDate] = useState(new Date());
  const [modalVisible, setModalVisible] = useState(false);//confirmacion
  const {id} =UserStore();
  const [isFocus, setIsFocus] = useState(false);//de dropdown
  const [data3,setdata3] =useState([])///users
  const [data4,setdata4] =useState([])///projects

//////////////////////////calendario
const onChange = (event, selectedDate, dateType) => {
  const currentDate = selectedDate || date;
  if (dateType === 'start') {
    setinitDate(currentDate);
  } else {
    setendDate(currentDate);
  }
};
//////////////////////////////////////////////////////////////
  const showMode = (currentMode, dateType) => {
  DateTimePickerAndroid.open({
    value: dateType === 'start' ? initDate : endDate,
    onChange: (event, selectedDate) => onChange(event, selectedDate, dateType),
    mode: currentMode,
    is24Hour: true,
  });
  };
  //////////////////////////////////////////////////////////////
  const showDatepicker = (dateType) => {
    showMode('date', dateType);
  };
  ///////////////////////////////////////////////////////
  
  const fecha_inicial = () => {
    showDatepicker('start');
  };
  const fecha_termino = () => {
    showDatepicker('end');
  };
 
  /////////////////////////////////////////////////////////////////////////////
  const Crear =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/tasks'
      const body ={
        "name": name,
        "description": description,
        "creatorID":id,//id de un usuario logeado
        "responsiblename": responsibleName,
        "startDate": format(initDate, 'yyyy-MM-dd'),
        "endDate": format(endDate, 'yyyy-MM-dd'),
        "projectname": proyectName
      }
      const response =await axios.post(url,body)
      Alert.alert('se ha creado una nueva tarea')
      navigation.navigate('mainValid')

    }catch(error){
      console.error('Error al realizar la solicitud:',error.response.data.message.toString())
      Alert.alert(error.response.data.message.toString())
    }
    handleCancelChanges();
    setModalVisible(false);
  };
  //////////////////////////////////////////////////////////////////////////////////////////////

  //no guardar cambios
  const handleCancelChanges = () => {
    setName('');
    setDescription('');
    setresponsibleName('');
    setproyectName('');
    setinitDate(new Date());
    setendDate(new Date())
    setModalVisible(false);
  };

  //esto es para cargar los datos sin depender de un boton
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
        const response2 = await axios.get(process.env.EXPO_PUBLIC_URL+'/projects');
        setdata3(response.data);
        setdata4(response2.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  //

  return (
    <KeyboardAwareScrollView  >
      
      <View style={styles.inner} >
      <Text style={styles.title}>Crear una nueva tarea</Text>

      <View style={styles.container}>

      <TextInput
        placeholder="Nombre de la tarea"
        style={styles.input}
        onChangeText={text => setName(text)}
        value={name}
      />

      <TextInput
        placeholder="descripcion"
        style={styles.input}
        onChangeText={text => setDescription(text)}
        value={description}
      />

      <Dropdown
        style={[styles.input, isFocus && { borderColor: 'blue' }]}
        data={data3}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={!isFocus ? 'Seleccione el responsable' : '...'}
        onChange={item => {
          setresponsibleName(item.name);///aqui se selecciona el campo para cambiar
          setIsFocus(false);
        }}
      />

      <TouchableOpacity style={styles.input} onPress={fecha_inicial} >
        <Text style={styles.text_button}>Inicio = {initDate.toLocaleDateString()}</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.input} onPress={fecha_termino} >
        <Text style={styles.text_button}>Termino = {endDate.toLocaleDateString()}</Text>
      </TouchableOpacity>

      <Dropdown
          style={[styles.input, isFocus && { borderColor: 'blue' }]}
          data={data4}
          search
          maxHeight={300}
          labelField="name"
          valueField="value"
          placeholder={!isFocus ? 'Select item' : '...'}
          searchPlaceholder="Search..."
          onFocus={() => setIsFocus(true)}
          onBlur={() => setIsFocus(false)}
          onChange={item => {
            setproyectName(item.name);
            setIsFocus(false);
          }}
        />

      <TouchableOpacity style={styles.button} onPress={() =>setModalVisible(true)} >
        <Text style={styles.buttonText}>Crear Tarea</Text>
      </TouchableOpacity>

      <TextInput>
        
      </TextInput>

      </View>
      </View>

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={Crear}
        onCancel={handleCancelChanges}
      />
      </KeyboardAwareScrollView>      
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    backgroundColor: 'orange',//color 
    alignItems: 'center',//alinea al centro
    justifyContent: 'center',
    paddingBottom:20
  },
  title: {
    fontSize: 24,
    marginBottom: 0,
    textDecorationLine: 'underline'
  },
  input: {
    borderWidth:1,
    borderColor:'blue',
    paddingStart:30,
    width: '80%',
    height:70,
    padding: 10,
    marginTop:30,//separacion entre las columnas
    borderRadius: 50,
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: 'blue',
    marginTop:20,
    padding: 10,
    borderRadius: 50,
    width: '50%',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  inner: {
    padding: 24,
    paddingBottom:130,
    flex: 1,
    justifyContent: 'center',
    backgroundColor:'orange'
  },
  text_button:{
    //color: 'white',
    fontSize: 18,
    textAlign: 'center',
    justifyContent:'center',
    //backgroundColor:'blue',
    marginEnd:28,
    marginTop:10
  },
 
 
});

export default PostTask;