//esta pagina te lleva a obtener todos los usuarios
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet,ScrollView, StatusBar, FlatList } from 'react-native';
import axios from 'axios';
import DesingGetUsers from '../user/DesingGetUsers';

const GetTaskFind = ({}) => {

  const [responseData, setResponseData] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
      setResponseData(response.data);
    } catch (error) {
      console.error('Error al realizar la solicitud:', error);
    }
  };

 
  return (
    <ScrollView style={styles.container}>
      
      <TouchableOpacity onPress={fetchData} style={styles.button}>
        <Text style={styles.buttonText}>Obtener Tareas</Text>
      </TouchableOpacity>
      
      <FlatList
        data={responseData}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>ID: {item.id}</Text>
            <Text>name: {item.name}</Text>
            <Text>rol: {item.roles}</Text>
          </View>
        )}
      />

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  responseText: {
    marginTop: 20,
    fontSize: 16,
  },
});

export default GetTaskFind;