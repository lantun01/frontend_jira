import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, StatusBar, TextInput } from 'react-native';
import axios from 'axios';
import DesingGetTask from './DesingGetTask';
import { UserStore } from '../../store/UserStore';
import { UserData } from '../../interfaces/userData';
import { TaskData } from '../../interfaces/taskData';
import { AxiosResponse } from 'axios';


const GetTask = ({navigation}) => {
  const [responseData, setResponseData] = useState<TaskData[]>([]);
  const [search, setSearch] = useState('');
  const [filteredData, setFilteredData] = useState<TaskData[]>([]);
  const [lista, setLista] = useState<string[]>([]);
  const {id} =UserStore();

  const fetchData = async () => {
    try {
      const response = await axios.get(process.env.EXPO_PUBLIC_URL + '/tasks');
      setResponseData(response.data);
      

      const reducedUserList: string[] = response.data.map((user: UserData) => user.name);
      setLista(reducedUserList);

      const filteredList = reducedUserList.filter(word =>
        word.toLowerCase().includes(search.toLowerCase())
      );
      setFilteredData(response.data.filter(user => filteredList.includes(user.name)));
    } catch (error) {
      console.error('Error al realizar la solicitud1:', error);
    }
  };

  const GetAll = async () => {
    try {
      const response :AxiosResponse<TaskData[]> = await axios.get(process.env.EXPO_PUBLIC_URL + '/tasks');
      setSearch('')
      setFilteredData(response.data);

    } catch (error) {
      console.error('Error al realizar la solicitud2:', error);
    }
  };

  const GetMyAll = async () => {
    try {
      // Obtener la lista completa de usuarios
      const response = await axios.get(process.env.EXPO_PUBLIC_URL + '/tasks');

      // Filtrar la lista para obtener solo los usuarios con el email correspondiente
      const filteredList = response.data.filter(creator => creator.id ===id);

      // Actualizar el estado de filteredData con la lista filtrada
      setFilteredData(filteredList);

      // Limpiar el campo de búsqueda
      setSearch('');
    } catch (error) {
      console.error('Error al realizar la solicitud3:', error);
    }
  };

  ///////////////////////////////////
  useEffect(() => {
    if (search) {
      fetchData();
    } else {
      setFilteredData(responseData); // Restaurar la lista original cuando no hay término de búsqueda
    }
  }, [search]);
  ////////////////////////////////////
  

  return (
    <ScrollView style={styles.container}>

      <View style={styles.container3} >
      <Text>Introduce la palabra</Text>
      <TextInput
        placeholder="nombre"
        onChangeText={(text) => setSearch(text)}
        value={search}
      />
      </View>

    
      <View style={styles.container2}>
      <View>
      <TouchableOpacity onPress={GetAll} style={styles.button}>
        <Text style={styles.buttonText}>Obtener Tareas</Text>
      </TouchableOpacity>
      </View>

      <View>
      <TouchableOpacity onPress={GetMyAll} style={styles.button}>
        <Text style={styles.buttonText}>Obtener mis Tareas</Text>
      </TouchableOpacity>
      </View>

      </View>

      {filteredData.map((data, i) => (
        <DesingGetTask key={i} data={data} />
      ))}
      
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#ffbc40'
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  },
  container2:{
    flexDirection: 'row', // Alineación horizontal
    justifyContent: 'space-around', // Espacio entre los componentes
    alignItems: 'center', // Centrar verticalmente
    marginTop:20
},
container3:{
  backgroundColor:'#fb4646be',
  marginTop:30,
  marginHorizontal:30,
  borderTopRightRadius:15,
  borderBottomLeftRadius:15
}
});

export default GetTask;