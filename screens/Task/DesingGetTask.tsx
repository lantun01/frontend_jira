import { Text, StyleSheet, TouchableOpacity, View } from "react-native";
import { TaskData } from "../../interfaces/taskData";

const DesingGetTask = ({ data, navigation }: { data: TaskData; navigation: any }) => {
  return (
    <View style={styles.item}>
      <Text style={styles.date}>ID={data.id}</Text>
      <Text style={styles.titulo}>Name={data.name}</Text>
      <Text style={styles.titulo}>descripcion={data.description}</Text>

      {data.creator ? (
        <Text style={styles.titulo}>Creado por={data.creator.name}</Text>
      ) : (
        <Text style={styles.titulo}>Creador no especificado</Text>
      )}

      {data.responsible ? (
        <Text style={styles.titulo}>Responsable={data.responsible.name}</Text>
      ) : (
        <Text style={styles.titulo}>Responsable no especificado</Text>
      )}

      <Text style={styles.titulo}>Inicio={data.startDate.slice(0, 10)}</Text>
      <Text style={styles.titulo}>Termino={data.endDate.slice(0, 10)}</Text>
      <Text style={styles.titulo}>Estado={data.taskStatus}</Text>

      {data.project ? (
        <Text style={styles.titulo}>Projecto ={data.project.name}</Text>
      ) : (
        <Text style={styles.titulo}>Projecto no especificado</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "orange",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderColor: "black",
    borderWidth: 1,
    marginTop: 15,
  },
  date: {
    marginBottom: 10,
    color: "blue",
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
  text_description: {
    padding: 10,
  },
  description: {
    borderWidth: 0.5,
    borderRadius: 5,
    backgroundColor: "purple",
  },
  button: {
    marginTop: 15,
    backgroundColor: "red",
    width: 100,
    borderRadius: 5,
  },
  text_button: {
    backgroundColor: "white",
    padding: 5,
    textAlign: "center",
  },
});

export default DesingGetTask;
