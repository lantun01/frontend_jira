import React, { useEffect, useState } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Octicons from 'react-native-vector-icons/Octicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import GetTask from "./GetTask";
import PostTask from "./PostTask";
import UpdateTask from "./UpdateTask";
import DeleteTask from "./DeleteTask";
import DesingGetTask from "./DesingGetTask";
import Page from "./Page";

//barra de navegacion horizontal
const tab =createBottomTabNavigator();

//icons antDesing
function TabBarIcon({name, color}){
    return (
        <AntDesign size={30} name={name} color={color}/>
    );
};
//icons FontAwesome
function TabBarIcon2({name, color}){
    return (
        <FontAwesome size={30} name={name} color={color}/>
    );
}

function TabBarIcon3({name, color}){
    return (
        <Octicons size={30} name={name} color={color}/>
    );
}
function TabBarIcon4({name, color}){
    return (
        <MaterialIcons size={30} name={name} color={color}/>
    );
}
function TabBarIcon5({name, color}){
    return (
        <MaterialCommunityIcons size={30} name={name} color={color}/>
    );
}

export default function MainTask({navigation}){
    return(
        <tab.Navigator
        >
            <tab.Screen name="GetTask" component={GetTask}  options={{
                tabBarLabel:'Lista Tareas',//titulo del boton
                headerShown:false,//visible el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon3 name='tasklist' color={'black'} />
                ),
                headerTintColor:"purple",//vuelve las letras de header moradas
                
             }} />
            <tab.Screen name="PostTask" component={PostTask} options={{
                tabBarLabel:'Crear Tarea',//titulo del boton
                headerShown:false,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon4 name='add-task' color={'black'} />
                ),
                //tabBarStyle:{display:"none"}//elimina el tabBar
             }} />
            <tab.Screen name="UpdateTask" component={UpdateTask} options={{
                tabBarLabel:'Actualizar Tarea',//titulo del boton
                headerShown:false,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon5 name='update' color={'black'} />
                ),
             }} />
            <tab.Screen name="DeleteTask" component={DeleteTask} options={{
                tabBarLabel:'Eliminar',//titulo del boton
                headerShown:false,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='delete' color={'black'} />
                ),
             }}  />

    
            
             
        </tab.Navigator>
    );
}