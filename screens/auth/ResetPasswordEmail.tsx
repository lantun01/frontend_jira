import React, { useState } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';
import LoadingButton from '../../components/LoadingButton';

const ResetPasswordEmailScreen: React.FC = () => {
  const [email, setEmail] = useState('');
  const navigation = useNavigation();

  const handleSendCode = async () => {
    if (!email.trim()) {
      return;
    }

    try {

      const response = await axios.patch(`${process.env.EXPO_PUBLIC_URL}/auth/reset`, { email });


      if (response.status === 200) {
        navigation.navigate('resetPasswordCode', { email });
      } else {
        const errorMessage =
          response.data.error || 'Hubo un problema al enviar el código. Por favor, verifica tu correo electrónico.';
      }
    } catch (error) {
      console.error('Error durante la solicitud:', error.message);
    }
  };

  const handleEmailChange = (text: string) => {
    setEmail(text.toLowerCase());
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Correo electrónico"
          value={email}
          onChangeText={handleEmailChange}
          autoCapitalize="none"
        />
      </View>
      <LoadingButton onPress={handleSendCode} title="Enviar Código" disabled={!email.trim()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: 'blue',
    paddingStart: 30,
    width: '80%',
    height: 50,
    padding: 10,
    borderRadius: 50,
    backgroundColor: 'white',
  },
});

export default ResetPasswordEmailScreen;
