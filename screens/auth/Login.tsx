import axios from "axios";
import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from "react-native";
import LoadingButton from "../../components/LoadingButton";
import isValidEmail from "../../utils/EmailValidator";
import { UserStore } from "../../store/UserStore";
import { RouteProp, NavigationProp } from "@react-navigation/native";
import { RootStackParamList } from "../../types/rootsStackParamList";

type LoginScreenRouteProp = RouteProp<RootStackParamList, "login">;
type LoginScreenNavigationProp = NavigationProp<RootStackParamList, "login">;

type Props = {
  route: LoginScreenRouteProp;
  navigation: LoginScreenNavigationProp;
};

const Login: React.FC<Props> = ({ navigation }) => {
  const { setAccessToken } = UserStore();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [passwordError, setPasswordError] = useState<string>("");
  const [emailError, setEmailError] = useState<string>("");
  const [emailTouched, setEmailTouched] = useState(false);
  const [passwordTouched, setPasswordTouched] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const emailInputRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);

  useEffect(() => {
    validateEmail();
    validatePassword();
    checkEnableButton();
  }, [email, password]);

  const validateEmail = () => {
    if (!emailTouched) {
      return;
    }

    if (!isValidEmail(email)) {
      setEmailError("Ingresa un correo electrónico válido");
    } else {
      setEmailError("");
    }
  };

  const validatePassword = () => {
    if (!passwordTouched) {
      return;
    }

    if (password.length < 6) {
      setPasswordError("La contraseña debe tener al menos 6 caracteres");
    } else {
      setPasswordError("");
    }
  };

  const checkEnableButton = () => {
    setIsButtonDisabled(!!emailError || !!passwordError);
  };
  const LoginRequest = async () => {
    try {
      setLoading(true);

      validateEmail();
      validatePassword();

      if (emailError || passwordError) {
        return;
      }

      const url = process.env.EXPO_PUBLIC_URL + "/auth/login";
      const body = {
        email: email.toLowerCase(),
        password: password,
      };
      const response = await axios.post(url, body);

      const accessToken = response?.data?.accesstoken || "";
      setAccessToken(accessToken);

      navigation.navigate("mainValid");
    } catch (error) {
      if (axios.isAxiosError(error) && error.response && error.response.data) {
        Alert.alert(error.response.data.message.toString());
      } else {
        Alert.alert("An error occurred");
      }
    } finally {
      setModalVisible(false);
      setLoading(false);
    }
  };

  const handleCancelChanges = () => {
    setEmail("");
    setPassword("");
    setModalVisible(false);
  };

  const handleResetPassword = () => {
    navigation.navigate("resetPasswordEmail");
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Iniciar sesión</Text>
      <TextInput
        ref={emailInputRef}
        placeholder="Correo electrónico"
        style={styles.input}
        onChangeText={(text) => setEmail(text)}
        value={email}
        autoCapitalize="none"
        returnKeyType="next"
        onBlur={() => setEmailTouched(true)}
        onSubmitEditing={() => {
          setPasswordTouched(true);
          passwordInputRef.current?.focus();
        }}
      />
      {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}
      <View style={styles.passwordInputContainer}>
        <TextInput
          ref={passwordInputRef}
          placeholder="Contraseña"
          secureTextEntry={!showPassword}
          style={styles.passwordInput}
          onChangeText={(text) => {
            setPassword(text);
            setPasswordError("");
          }}
          onBlur={() => setPasswordTouched(true)}
          value={password}
          returnKeyType="done"
          onSubmitEditing={LoginRequest}
        />
        <TouchableOpacity
          style={styles.passwordVisibilityButton}
          onPress={() => setShowPassword(!showPassword)}
        >
          <Text>{showPassword ? "Ocultar" : "Mostrar"}</Text>
        </TouchableOpacity>
      </View>
      {passwordError ? (
        <Text style={styles.errorText}>{passwordError}</Text>
      ) : null}
      <LoadingButton
        onPress={LoginRequest}
        title="Iniciar sesión"
        disabled={!isValidEmail(email) || password.length < 6}
      />
      <TouchableOpacity onPress={handleResetPassword}>
        <Text style={styles.resetPasswordText}>
          ¿Olvidaste tu contraseña? Restablecer
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 50,
    color: "black",
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "blue",
    paddingStart: 30,
    width: "80%",
    height: 50,
    padding: 10,
    marginTop: 20,
    borderRadius: 50,
    backgroundColor: "white",
  },
  passwordInputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "80%",
    marginTop: 20,
  },
  passwordInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: "blue",
    height: 50,
    padding: 10,
    borderRadius: 50,
    backgroundColor: "white",
  },
  passwordVisibilityButton: {
    padding: 10,
  },
  errorText: {
    color: "red",
    marginTop: 5,
  },
  resetPasswordText: {
    color: "blue",
    marginTop: 10,
    textDecorationLine: "underline",
  },
});

export default Login;

