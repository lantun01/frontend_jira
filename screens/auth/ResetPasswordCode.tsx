import React, { useState, useRef } from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";
import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import axios from "axios";
import LoadingButton from "../../components/LoadingButton";
import { RootStackParamList } from '../../types/rootsStackParamList';

type ResetPasswordCodeScreenRouteProp = RouteProp<
  RootStackParamList,
  "resetPasswordCode"
>;

type ResetPasswordCodeScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  "resetPasswordCode"
>;

type Props = {
  route: ResetPasswordCodeScreenRouteProp;
  navigation: ResetPasswordCodeScreenNavigationProp;
};

const ResetPasswordCodeScreen: React.FC<Props> = ({ navigation, route }) => {
  const { email } = route.params;
  const [code, setCode] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const isLoadingRef = useRef(isLoading);

  const handleVerifyCode = async () => {
    try {
      if (isLoadingRef.current) {
        return;
      }

      
      setIsLoading(true);
      setError(null);

      const response = await axios.post(
        `${process.env.EXPO_PUBLIC_URL}/auth/compare-code`,
        {
          email,
          resetCode: code,
        }
      );


      if (response.data.success) {
       
        navigation.navigate("resetPasswordChange", { email });
      } else {
        const errorMessage =
          response.data.error ||
          "Hubo un problema al verificar el código. Por favor, inténtalo de nuevo.";
        
        setError(errorMessage);
      }
    } catch (error) {
      console.error("Error durante la solicitud:", error);
      setError(
        "Hubo un error durante la verificación. Por favor, inténtalo de nuevo."
      );
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Ingresa el código de verificación enviado a:
      </Text>
      <Text style={styles.email}>{email}</Text>
      <TextInput
        style={styles.input}
        placeholder="Código"
        value={code}
        onChangeText={(text) => {
          if (text.length <= 8) {
            setCode(text);
          }
        }}
        maxLength={8}
        autoCapitalize="characters"
      />
      {error && <Text style={styles.errorText}>{error}</Text>}
      <LoadingButton
        onPress={handleVerifyCode}
        title="Verificar Código"
        disabled={!code.trim() || isLoading}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 18,
    color: "black",
    textAlign: "center",
    marginBottom: 10,
  },
  email: {
    fontSize: 16,
    color: "blue",
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: "blue",
    paddingStart: 30,
    width: "80%",
    height: 50,
    padding: 10,
    borderRadius: 50,
    backgroundColor: "white",
    marginTop: 20,
  },
  errorText: {
    color: "red",
    marginTop: 5,
  },
});

export default ResetPasswordCodeScreen;
