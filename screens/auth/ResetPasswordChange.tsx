import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, StyleSheet, Alert } from 'react-native';
import axios from 'axios';
import { useRoute, useNavigation } from '@react-navigation/native';
import LoadingButton from '../../components/LoadingButton';

const ResetPasswordChangeScreen: React.FC = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const { email } = route.params;
  const [newPassword, setNewPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');

  // Asegurarse de que el email esté presente y sea válido
  useEffect(() => {
    if (!email || typeof email !== 'string' || !email.trim()) {
      // Manejar el caso en que el email no está presente o no es válido
      Alert.alert('Error', 'No se proporcionó un email válido.');
      // Puedes redirigir a otra pantalla o realizar otras acciones según tus necesidades
      navigation.navigate('login');
    }
  }, [email, navigation]);

  const validatePassword = () => {
    if (newPassword.length < 6) {
      setPasswordError('La contraseña debe tener al menos 6 caracteres');
    } else {
      setPasswordError('');
    }
  };

  const handleChangePassword = async () => {
    try {
      validatePassword();

      if (passwordError) {
        return;
      }

      const response = await axios.post(`${process.env.EXPO_PUBLIC_URL}/auth/change-password`, {
        email,
        newPassword,
      });

      if (response.data.success) {
        // Éxito al cambiar la contraseña, navegar a 'login' en lugar de 'mainValid'
        navigation.navigate('login');
      } else {
        // Manejar errores específicos si es necesario
        Alert.alert('Error', 'Hubo un problema al cambiar la contraseña. Por favor, inténtalo de nuevo.');
      }
    } catch (error) {
      console.error('Error changing password:', error);

      if (error.response) {
        // Log the detailed error for debugging
        console.error('Detailed error:', error.response.data);
      }

      // Manejar errores específicos si es necesario
      Alert.alert('Error', 'Hubo un error durante la solicitud. Por favor, inténtalo de nuevo.');
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Cambia tu contraseña para {email}</Text>
      <TextInput
        style={styles.input}
        placeholder="Nueva Contraseña"
        value={newPassword}
        onChangeText={(text) => setNewPassword(text)}
        secureTextEntry
        onBlur={validatePassword}
      />
      {passwordError ? <Text style={styles.errorText}>{passwordError}</Text> : null}
      <LoadingButton title="Cambiar Contraseña" onPress={handleChangePassword} disabled={!newPassword.trim()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'orange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 18,
    color: 'black',
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: 'blue',
    paddingLeft: 15,
    width: '80%',
    height: 50,
    borderRadius: 10,
    backgroundColor: 'white',
    marginTop: 20,
  },
  errorText: {
    color: 'red',
    marginTop: 5,
  },
});

export default ResetPasswordChangeScreen;
