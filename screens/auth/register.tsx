import axios, { AxiosError } from "axios";
import React, { useState, useRef } from "react";
import { View, Text, TextInput, StyleSheet, Alert } from "react-native";
import {
  useNavigation,
  NavigationProp,
  RouteProp,
} from "@react-navigation/native";
import LoadingButton from "../../components/LoadingButton";
import ConfirmationModal from "../../components/modalConfirmacion";
import { UserStore } from "../../store/UserStore";
import isValidEmail from "../../utils/EmailValidator";
import { RootStackParamList } from "../../types/rootsStackParamList";

type RegisterScreenRouteProp = RouteProp<RootStackParamList, "registro">;
type RegisterScreenNavigationProp = NavigationProp<
  RootStackParamList,
  "registro"
>;

const RegisterScreen = () => {
  const { setAccessToken } = UserStore();
  const navigation = useNavigation<RegisterScreenNavigationProp>();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [modalVisible, setModalVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const emailInputRef = useRef<TextInput>(null);
  const passwordInputRef = useRef<TextInput>(null);

  const validateEmail = () => {
    if (!isValidEmail(email)) {
      setEmailError("Ingresa un correo electrónico válido");
    } else {
      setEmailError("");
    }
  };

  const validatePassword = () => {
    if (password.length < 6) {
      setPasswordError("La contraseña debe tener al menos 6 caracteres");
    } else {
      setPasswordError("");
    }
  };

  const Register = async () => {
    try {
      setLoading(true);
      validateEmail();
      validatePassword();

      if (emailError || passwordError) {
        return;
      }

      const url = process.env.EXPO_PUBLIC_URL + "/auth/register";
      const body = {
        name: name,
        email: email.toLowerCase(),
        password: password,
      };

      const response = await axios.post(url, body);
      const accessToken = response?.data?.accesstoken || "";
      setAccessToken(accessToken);
      navigation.navigate("mainValid");
    } catch (error) {
      handleApiError(error);
    } finally {
      setModalVisible(false);
      setLoading(false);
    }
  };

  const handleApiError = (error: unknown) => {
    if (axios.isAxiosError(error)) {
      const axiosError = error as AxiosError;
      if (axiosError.response && axiosError.response.data) {
        Alert.alert(axiosError.response.data.message.toString());
      } else {
        Alert.alert("An error occurred");
      }
    } else {
      Alert.alert("An unknown error occurred");
    }
  };

  const handleCancelChanges = () => {
    setName("");
    setEmail("");
    setPassword("");
    setModalVisible(false);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Registrar un nuevo usuario</Text>

      <TextInput
        placeholder="Nombre"
        style={styles.input}
        onChangeText={(text) => setName(text)}
        value={name}
        returnKeyType="next"
        onSubmitEditing={() => emailInputRef.current?.focus()}
      />

      <TextInput
        ref={emailInputRef}
        placeholder="Correo electrónico"
        style={styles.input}
        onChangeText={(text) => setEmail(text)}
        value={email}
        autoCapitalize="none"
        returnKeyType="next"
        onBlur={validateEmail}
        onSubmitEditing={() => passwordInputRef.current?.focus()}
      />
      {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}

      <TextInput
        ref={passwordInputRef}
        placeholder="Contraseña"
        secureTextEntry
        style={styles.input}
        onChangeText={(text) => {
          setPassword(text);
          setPasswordError("");
        }}
        onBlur={validatePassword}
        value={password}
        returnKeyType="done"
        onSubmitEditing={Register}
      />
      {passwordError ? (
        <Text style={styles.errorText}>{passwordError}</Text>
      ) : null}

      <LoadingButton
        onPress={Register}
        title="Registrar"
        disabled={!name.trim() || !isValidEmail(email) || password.length < 6}
      />

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={Register}
        onCancel={handleCancelChanges}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 50,
    color: "black",
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "blue",
    paddingStart: 30,
    width: "80%",
    height: 50,
    padding: 10,
    marginTop: 20,
    borderRadius: 50,
    backgroundColor: "white",
  },
  errorText: {
    color: "red",
    marginTop: 5,
  },
});

export default RegisterScreen;
