import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AntDesign, Octicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import GetProyect from "./GetProyect";
import PostProyect from "./PostProyect";
import UpdateProyect from "./UpdateProyect";
import DeleteProyect from "./DeleteProyect";

const Tab = createBottomTabNavigator();

type TabIconProps = { name: string; color: string };

const ListIcon: React.FC<TabIconProps> = ({ name, color }) => <Octicons size={30} name={name as any} color={color} />;
const CreateIcon: React.FC<TabIconProps> = ({ name, color }) => <MaterialIcons size={30} name={name as any} color={color} />;
const UpdateIcon: React.FC<TabIconProps> = ({ name, color }) => <MaterialCommunityIcons size={30} name={name as any} color={color} />;
const DeleteIcon: React.FC<TabIconProps> = ({ name, color }) => <AntDesign size={30} name={name as any} color={color} />;

export default function MainTask() {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="obtener todos los Proyectos"
        component={GetProyect}
        options={{
          tabBarLabel: 'Lista Proyectos',
          headerShown: false,
          tabBarIcon: ({ color }) => <ListIcon name="list-ordered" color={color} />,
          headerTintColor: "purple",
        }}
      />
      <Tab.Screen
        name="PostProject"
        component={PostProyect}
        options={{
          tabBarLabel: 'Crear Proyecto',
          headerShown: false,
          tabBarIcon: ({ color }) => <CreateIcon name="add" color={color} />,
        }}
      />
      <Tab.Screen
        name="UpdateProject"
        component={UpdateProyect}
        options={{
          tabBarLabel: 'Actualizar Proyecto',
          headerShown: false,
          tabBarIcon: ({ color }) => <UpdateIcon name="update" color={color} />,
        }}
      />
      <Tab.Screen
        name="DeleteProject"
        component={DeleteProyect}
        options={{
          tabBarLabel: 'Eliminar',
          headerShown: false,
          tabBarIcon: ({ color }) => <DeleteIcon name="delete" color={color} />,
        }}
      />
    </Tab.Navigator>
  );
}
