import React from "react";
import { Text, StyleSheet, View } from "react-native";

interface ProjectData {
  id: number;
  name: string;
  description: string;
}

const DesingGetProjects: React.FC<{ data: ProjectData }> = ({ data }) => {
  return (
    <View style={styles.item}>
      <Text style={styles.date}>ID={data.id}</Text>
      <Text style={styles.titulo}>Name={data.name}</Text>
      <Text style={styles.titulo}>descripcion={data.description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "orange",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderColor: "black",
    borderWidth: 1,
    marginTop: 15,
  },
  date: {
    marginBottom: 10,
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
});

export default DesingGetProjects;
