import axios, { AxiosError } from 'axios';
import { Alert, Text, TextInput, TouchableOpacity, View, StyleSheet } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../../types/rootsStackParamList';
import { useState } from 'react';
type PostProyectProps = {
  navigation: StackNavigationProp<RootStackParamList, 'main_proyect'>;
};

const PostProyect: React.FC<PostProyectProps> = ({ navigation }) => {
  const [name, setName] = useState('');
  const [description, setdescription] = useState('');

  const Crear = async () => {
    try {
      const url = process.env.EXPO_PUBLIC_URL + '/projects';
      const body = {
        name: name,
        description: description,
      };
      const response = await axios.post(url, body);
      Alert.alert('Se ha creado un nuevo proyecto');
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        const errorMessage = axiosError.response?.data?.message || 'Hubo un error al realizar la solicitud.';
        Alert.alert(errorMessage);
      } else {
        console.error('Error desconocido:', error);
        Alert.alert('Hubo un error al realizar la solicitud.');
      }
    }
    navigation.navigate('mainValid');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Crear un nuevo proyecto</Text>
      <TextInput
        placeholder="Nombre del proyecto"
        style={styles.input}
        onChangeText={(text) => setName(text)}
      />
      <TextInput
        placeholder="Descripcion"
        style={styles.input}
        onChangeText={(text) => setdescription(text)}
      />
      <TouchableOpacity style={styles.button} onPress={Crear}>
        <Text style={styles.buttonText}>Crear</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default PostProyect;


