import axios, { AxiosError } from 'axios';
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';
import ConfirmationModal from '../../components/modalConfirmacion';
import { RootStackParamList } from '../../types/rootsStackParamList';
import { StackNavigationProp } from '@react-navigation/stack';

type UpdateProyectProps = {
  navigation: StackNavigationProp<RootStackParamList, 'main_proyect'>;
};

const UpdateProyect: React.FC<UpdateProyectProps> = ({ navigation }) => {
  const [idMain, setidMain] = useState<number>(0);//id del proyecto
  const [name, setName] = useState('');
  const [description, setdescription] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [isFocus, setIsFocus] = useState(true);//de dropdown
  const [data,setdata] =useState([])///data de proyectos
  const [modalVisible, setModalVisible] = useState(false);//confirmacion

  const Update =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/projects'
      const body ={
        "id": idMain,
        "name": name,
        "description": description,
      }
      const response =await axios.put(url,body)
      Alert.alert('se ha actualizado un proyecto')
      navigation.navigate('mainValid')

    }catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        console.error('Error al realizar la solicitud:', axiosError.response?.data?.message);
        Alert.alert(axiosError.response?.data?.message || 'Hubo un error en la solicitud.');
      } else {
        console.error('Error al realizar la solicitud:', error);
        Alert.alert('Hubo un error en la solicitud.');
      }
    }
    handleCancelChanges();
    setModalVisible(false);
  };
  //////////////////////////////////////////////////////////////////////////////////////////////
   //no guardar cambios
   const handleCancelChanges = () => {
    setName('');
    setdescription('');
    setModalVisible(false);
  };

  //esto es para cargar los datos sin depender de un boton
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/projects');
        setdata(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  //

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Actualizar proyecto</Text>

      <Dropdown
        style={[styles.input, isFocus && { borderColor: 'blue' }]}
        data={data}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={!isFocus ? 'Seleccione el proyecto actualizar' : '...'}
        onChange={item => {
          setidMain(item.id);///aqui se selecciona el campo para cambiar
          setIsFocus(false);
        }}
      />

      <TextInput
        placeholder="Nombre del proyecto"
        style={styles.input}
        onChangeText={text => setName(text)}
      />

      <TextInput
        placeholder="descripcion"
        style={styles.input}
        onChangeText={text => setdescription(text)}
      />

      <TouchableOpacity style={styles.button} onPress={() =>setModalVisible(true)} >
        <Text style={styles.buttonText}>Actualizar</Text>
      </TouchableOpacity>

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={Update}
        onCancel={handleCancelChanges}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default UpdateProyect;