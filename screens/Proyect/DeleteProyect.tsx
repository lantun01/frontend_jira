import axios, { AxiosError, AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from "react-native";
import { Dropdown } from "react-native-element-dropdown";
import ConfirmationModal from "../../components/modalConfirmacion";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../types/rootsStackParamList";

interface Project {
  id: number;
  name: string;
}

const DeleteProject: React.FC<{
  navigation: StackNavigationProp<RootStackParamList, "mainValid">;
}> = ({ navigation }) => {
  const [idMain, setidMain] = useState<number | null>(null);
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [isFocus, setIsFocus] = useState(true);
  const [data, setdata] = useState<Project[]>([]);
  const [modalVisible, setModalVisible] = useState(false);

  const updateProject = async () => {
    try {
      if (idMain === null) {
        Alert.alert("Por favor, seleccione un proyecto");
        return;
      }

      const url = `${process.env.EXPO_PUBLIC_URL}/projects`;
      const body = {
        id: idMain,
        visible: false,
      };

      const response: AxiosResponse = await axios.put(url, body);
      Alert.alert("Se ha eliminado un proyecto");
      navigation.navigate("mainValid");
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError; // Hacemos un cast a AxiosError
        console.error(
          "Error al realizar la solicitud:",
          axiosError.response?.data?.message
        );
        Alert.alert(
          axiosError.response?.data?.message ||
            "Hubo un error al eliminar el proyecto."
        );
      } else {
        console.error("Error al realizar la solicitud:", error);
        Alert.alert("Hubo un error al eliminar el proyecto.");
      }
    } finally {
      handleCancelChanges();
      setModalVisible(false);
    }
  };

  const handleCancelChanges = () => {
    setModalVisible(false);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response: AxiosResponse<Project[]> = await axios.get(
          `${process.env.EXPO_PUBLIC_URL}/projects`
        );
        setdata(response.data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Eliminar proyecto</Text>

      <Dropdown
        style={[styles.input, isFocus && { borderColor: "blue" }]}
        data={data}
        maxHeight={300}
        labelField="name"
        valueField="id"
        placeholder={!isFocus ? "Seleccione el proyecto actualizar" : "..."}
        onChange={(item) => {
          setidMain(item.id);
          setIsFocus(false);
        }}
      />

      <TouchableOpacity
        style={styles.button}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.buttonText}>Eliminar</Text>
      </TouchableOpacity>

      <ConfirmationModal
        isVisible={modalVisible}
        message="¿Desea guardar estos cambios?"
        onConfirm={updateProject}
        onCancel={handleCancelChanges}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f0f0f0",
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: "80%",
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: "blue",
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    textAlign: "center",
  },
});

export default DeleteProject;
