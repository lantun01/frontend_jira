import React, { useState, useEffect } from 'react';
import { View, Text, Button, TextInput, FlatList,StyleSheet  } from 'react-native';
import axios from 'axios';
import { router } from 'expo-router';
import { Dropdown } from 'react-native-element-dropdown';

const Buscar_Usuarios_teams = ({navigation}) => {
  const [data, setData] = useState(null);
  const [id, setId] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [data2,setdata2] =useState([])
  const [data1,setdata1] =useState([])

  const fetchData = async () => {
    setLoading(true);
    setError(null);
    setData(null);

    try {
      const response = await axios.get(process.env.EXPO_PUBLIC_URL+`/team/users/${id}`);
      setData(response.data);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  };


  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/team');
        const response2 = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
        setdata2(response.data);
        setdata1(response2.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
 
  return (
    <View>
      <Text>Nombre del equipo</Text>
      <Dropdown
        style={[styles.input,{ borderColor: 'blue' }]}
        data={data2}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={'Seleccione el responsable'}
        onChange={item => {
          setId(item.id);///aqui se selecciona el campo para cambiar
        }}
      />
      <Button title="Obtener Usuarios" onPress={fetchData} />

      {loading && <Text>Cargando...</Text>}

      {error && <Text>Error: {error.message}</Text>}

      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>ID: {item.id}</Text>
            <Text>name: {item.name}</Text>
            <Text>rol: {item.roles}</Text>
          </View>
        )}
      />

      
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});
export default Buscar_Usuarios_teams;