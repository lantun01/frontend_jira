
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Entypo from 'react-native-vector-icons/Entypo';

import GetTeams from "./GetTeams";
import PostTeam from "./PostTeam";
import UpdateTeam from "./UpdateTeam";
import DeleteTeam from "./DeleteTeam";
import Buscar_Usuarios_teams from "./Buscar_Usuarios_teams";
import AsignarUsuario from "./Asignar_Usuario_Team";
import { background } from "native-base/lib/typescript/theme/styled-system";
//barra de navegacion horizontal
const tab =createBottomTabNavigator();

//icons
function TabBarIcon({name, color}){
    return (
        <AntDesign size={30} name={name} color={color}/>
    );
};
function TabBarIcon2({name, color}){
    return (
        <FontAwesome size={30} name={name} color={color}/>
    );
}
function TabBarIcon3({name, color}){
    return (
        <MaterialCommunityIcons size={30} name={name} color={color}/>
    );
}
function TabBarIcon4({name, color}){
    return (
        <Entypo size={30} name={name} color={color}/>
    );
}

export default function MainTeam(){
    return(
        <tab.Navigator>
            <tab.Screen name="obtener todos los teams" component={GetTeams} options={{
                tabBarLabel:'Teams',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='team' color={'green'} />
                ),
             }} />
            <tab.Screen name="Crear nuevo Team" component={PostTeam} options={{
                tabBarLabel:'Add Team',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon2 name='group' color={'green'} />
                ),
             }}  />
            <tab.Screen name="update" component={UpdateTeam} options={{
                tabBarLabel:'Update',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon3 name='update' color={'green'} />
                ),
             }}  />
            <tab.Screen name="Buscar Usuarios" component={Buscar_Usuarios_teams} options={{
                tabBarLabel:'Find',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon4 name='magnifying-glass' color={'green'} />
                ),
             }}  />
            <tab.Screen name="Asignar Usuario" component={AsignarUsuario} options={{
                tabBarLabel:'Add User',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='addusergroup' color={'green'} />
                ),
             }} />
            <tab.Screen name="Delete" component={DeleteTeam} options={{
                tabBarLabel:'Delete team',//titulo del boton
                headerShown:true,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='deleteusergroup' color={'green'} />
                ),
             }}  />
            
        </tab.Navigator>
    );
}