import React, { useState } from 'react';
import { View, Text, Button, TextInput } from 'react-native';
import axios from 'axios';

const DeleteTeam = () => {
  const [url, setUrl] = useState(''); // Estado para almacenar la URL ingresada
  const [isDeleted, setIsDeleted] = useState(false);
  const [error, setError] = useState(null);
  const urlG=process.env.EXPO_PUBLIC_URL+'/team/'


  const handleDelete = () => {
    const url2=urlG+url;
    axios
      .delete(url2) // Usar la URL ingresada
      .then(() => {
        setIsDeleted(true);
      })
      .catch((err) => {
        setError(err);
      });
  };

  return (
    <View>
      <Text>ingrese el id del team a Eliminar</Text>
      <TextInput
        placeholder="id del equipo"
        onChangeText={(text) => setUrl(text)} // Actualizar el estado 'url' con la URL ingresada
        value={url}
      />
      {isDeleted ? (
        <Text>El objeto ha sido eliminado con éxito.</Text>
      ) : (
        <Button title="Eliminar equipo" onPress={handleDelete} />
      )}
      {error && <Text>Error: {error.message}</Text>}
    </View>
  );
};


export default DeleteTeam;