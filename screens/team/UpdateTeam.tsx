import axios from 'axios';
import { router } from 'expo-router';
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';

const UpdateTeam = ({navigation}) => {
  const [name, setName] = useState('');
  const [id, setId] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [data,setdata] =useState([])
  const [data1,setdata1] =useState([])


  const Update =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/team/'+id
      const body ={
        "name": name,//nombre del team
      }
      const response =await axios.patch(url,body)
      Alert.alert('el equipo fue actualizado con exito')
      navigation.navigate('mainValid')

    }catch(error){
      //console.error('Error al realizar la solicitud:',error.response.data.message.toString())
      Alert.alert(error.response.data.message.toString())
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/team');
        const response2 = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
        setdata(response.data);
        setdata1(response2.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Actualizar equipo</Text>
      <Dropdown
        style={[styles.input,{ borderColor: 'blue' }]}
        data={data}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={'Seleccione el responsable'}
        onChange={item => {
          setId(item.id);///aqui se selecciona el campo para cambiar
        }}
      />
      <TextInput
        placeholder="Nuevo nombre de equipo"
        style={styles.input}
        onChangeText={text => setName(text)}
      />
      <TouchableOpacity style={styles.button} onPress={Update} >
        <Text style={styles.buttonText}>Actualizar</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default UpdateTeam;