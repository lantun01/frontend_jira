import axios from 'axios';
import { router } from 'expo-router';
import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import { Dropdown } from 'react-native-element-dropdown';

const AsignarUsuario = ({navigation}) => {
 
  const [iduser, setidUser] = useState('');
  const [idteam, setidTeam] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [data,setdata] =useState([])
  const [data1,setdata1] =useState([])


  const insertar =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/team/party'
      const body ={
        "id_user": parseInt(iduser),
        "id_team": parseInt(idteam),
      }
      const response =await axios.post(url,body)
      Alert.alert('Usuario agregado al team')
      navigation.navigate('mainValid')
    }catch(error){
      Alert.alert(error.response.data.message.toString())
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/team');
        const response2 = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
        setdata(response.data);
        setdata1(response2.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  
  return (
    <View style={styles.container}>
      <Text style={styles.title}>asignar usuario al equipo</Text>
      <Dropdown
        style={[styles.input,{ borderColor: 'blue' }]}
        data={data1}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={'Seleccione el responsable'}
        onChange={item => {
          setidUser(item.id);///aqui se selecciona el campo para cambiar
        }}
      />
      <Dropdown
        style={[styles.input,{ borderColor: 'blue' }]}
        data={data}//selecciona la data
        maxHeight={300}
        labelField="name"///selecciona el campo de busqueda
        valueField="palabra"//nose para que es esto
        placeholder={'Seleccione el responsable'}
        onChange={item => {
          setidTeam(item.id);///aqui se selecciona el campo para cambiar
        }}
      />
      <TouchableOpacity style={styles.button} onPress={insertar} >
        <Text style={styles.buttonText}>insertar</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default AsignarUsuario;
