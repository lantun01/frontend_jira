import { StackNavigationProp } from '@react-navigation/stack';
import axios from 'axios';
import { router, useNavigation } from 'expo-router';
import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import { RootStackParamList } from '../../types/rootsStackParamList';

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList>;

interface HomeProps {
  navigation: HomeScreenNavigationProp;
}

const PostTeam : React.FC<HomeProps> = ({ navigation })  => {
  const [name, setName] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const Crear =async () =>{
    try{
      const url = process.env.EXPO_PUBLIC_URL+'/team'
      const body ={
        "name": name,//nombre del team
      }
      const response =await axios.post(url,body)
      navigation.navigate("mainValid")
      Alert.alert('se ha creado un nuevo Team')
      

    }catch(error){
      //console.error('Error al realizar la solicitud:',error.response.data.message.toString())
      Alert.alert(error.response.data.message.toString())
    }
  };
  
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Crear un nuevo equipo</Text>
      <TextInput
        placeholder="Nombre del equipo"
        style={styles.input}
        onChangeText={text => setName(text)}
      />
      <TouchableOpacity style={styles.button} onPress={Crear} >
        <Text style={styles.buttonText}>Crear</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    paddingLeft: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default PostTeam;
