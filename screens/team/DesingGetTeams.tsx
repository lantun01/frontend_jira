import { Text ,StyleSheet,TouchableOpacity,View} from "react-native";
const DesingGetTeams = ({data})=>{

    return(
        <View style={styles.item}>
            <Text style={styles.date}>ID={data.id}</Text>
            <Text style={styles.titulo}>Name={data.name}</Text>
        </View>       
    )
};
const styles =StyleSheet.create({
    item:{
        backgroundColor: "orange",
        padding:20,
        marginVertical:8,
        marginHorizontal:16,
        borderColor:'black',
        borderWidth:1,
        marginTop:15,
    },
    date:{
        marginBottom:10,
    },
    titulo:{
        fontWeight:"bold",
        fontSize:20,
        marginBottom:10,
    },
    text_description:{
        padding:10,
    },
    description:{
        borderWidth:0.5,
        borderRadius:5,
        backgroundColor:'purple'
    },
    button:{
        marginTop:15,
        backgroundColor:'red',
        width:100,
        borderRadius:5
    },
    text_button:{
        backgroundColor:"white",
        padding:5,
        textAlign:"center"
    }
});

export default DesingGetTeams;