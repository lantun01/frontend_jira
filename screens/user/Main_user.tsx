
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

//screens,agregar las pantallas relacionadas a user
import Profile from "./profile";
///
import GetUsers from "./GetUsers";

//barra de navegacion horizontal
const tab =createBottomTabNavigator();

//icons antDesing
function TabBarIcon({name, color}){
    return (
        <AntDesign size={30} name={name} color={color}/>
    );
};
//icons FontAwesome
function TabBarIcon2({name, color}){
    return (
        <FontAwesome size={30} name={name} color={color}/>
    );
}

export default function MainUser(){
    return(
        <tab.Navigator >
            <tab.Screen name="Usuarios" component={GetUsers}  options={{
                tabBarLabel:'usuario',//titulo del boton
                headerShown:false,//visible el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='user' color={'blue'} />
                ),
                headerTintColor:"purple",//vuelve las letras de header moradas
             }} />
           
            <tab.Screen name="profile" component={Profile} options={{
                tabBarLabel:'perfil',//titulo del boton
                headerShown:false,//esto es para dejar o no el titulo
                tabBarIcon: ({color}) =>(
                    <TabBarIcon name='idcard' color={'blue'} />
                ),
             }}  />
        </tab.Navigator>
    );
}
   
