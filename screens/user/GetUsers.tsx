//esta pagina te lleva a obtener todos los usuarios
import React, { useState } from 'react';
import { Text, TouchableOpacity, StyleSheet,ScrollView } from 'react-native';
import axios from 'axios';
import DesingGetUsers from './DesingGetUsers';

const GetUsers = ({navigation}) => {
  const [responseData, setResponseData] = useState([]);

  const fetchData = async () => {
    try {
      const response = await axios.get(process.env.EXPO_PUBLIC_URL+'/users');
      setResponseData(response.data);
    } catch (error) {
      console.error('Error al realizar la solicitud:', error);
    }
  };
 
  return (
    <ScrollView style={styles.container}>

      <TouchableOpacity onPress={fetchData} style={styles.button}>
        <Text style={styles.buttonText}>Obtener Usuarios</Text>
      </TouchableOpacity>

      {
        responseData.map((data,i) =>{
          return <DesingGetUsers key={i} data={data} />
          })
      }

    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  responseText: {
    marginTop: 20,
    fontSize: 16,
  },
});

export default GetUsers;
