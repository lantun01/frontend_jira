import React from "react";
import { Text, StyleSheet, TouchableOpacity, View } from "react-native";

interface UserData {
  id: number;
  name: string;
  email: string;
  password: string;
}

interface DesingGetUsersProps {
  data: UserData;
  onDeleteTeam: (id: number) => void;
  onEditTeam: (id: number) => void;
}

const DesingGetUsers: React.FC<DesingGetUsersProps> = ({ data, onDeleteTeam, onEditTeam }) => {
  const handleDeleteTeam = () => {
    onDeleteTeam(data.id);
  };

  const handleEditTeam = () => {
    onEditTeam(data.id);
  };

  return (
    <View style={styles.item}>
      <Text style={styles.titulo}>Name={data.name}</Text>
      <Text style={styles.titulo}>Email={data.email}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: "orange",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderColor: 'black',
    borderWidth: 1,
    marginTop: 15,
  },
  date: {
    marginBottom: 10,
  },
  titulo: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
  text_button: {
    backgroundColor: "white",
    padding: 5,
    textAlign: "center"
  },
  button: {
    marginTop: 15,
    backgroundColor: 'red',
    width: 150,
    borderRadius: 5
  },
});

export default DesingGetUsers;
