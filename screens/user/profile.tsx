import React, { useEffect, useRef, useState } from "react";
import { View,StyleSheet ,Text,TextInput,SafeAreaView, Alert} from 'react-native';
import { UserStore } from '../../store/UserStore';
import { interfaceJWT } from "../../interfaces/JWT";
import jwtDecode from "jwt-decode";
import ConfirmationModal from '../../components/modalConfirmacion';
import isValidEmail from '../../utils/EmailValidator';
import axios from "axios";
import LoadingButton from "../../components/LoadingButton";
import {AntDesign} from '@expo/vector-icons';


const Profile = ({navigation}) =>{

    const { accessToken,setAccessToken } = UserStore();
    const {id,setId} =UserStore();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [loading, setLoading] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const emailInputRef = useRef<TextInput>(null);

  const validateEmail = () => {
    if (!isValidEmail(email)) {
      setEmailError('Ingresa un correo electrónico válido');
    } else {
      setEmailError('');
    }
  };

    const Register = async () => {
      setLoading(true);
      validateEmail();

      if (emailError) {
        return;
      }
        try {
          const url = process.env.EXPO_PUBLIC_URL + '/users';
          const body = {
            id: id,
            name: name,
            email: email.toLowerCase(),
          };
    
          const response = await axios.put(url,body);
          const accessToken = response?.data?.accesstoken || "";
          setAccessToken(accessToken); 
          Alert.alert('se ha actualizado el user')
          navigation.navigate('mainValid');
        } catch (error) {
          Alert.alert(error.response.data.message.toString());
        } finally {
          setModalVisible(false);
          setLoading(false);
        }
      };

    const handleCancelChanges = () => {
        setName('');
        setEmail('');
        setModalVisible(false);
    };

    //esto es para cargar el use_store
    useEffect(() => {
        if (accessToken === undefined) {
        } else {
          const decoded: interfaceJWT = jwtDecode(accessToken);
          setEmail(decoded.email)//guardo email
          setId(decoded.id)//guardo id
        }
      }, [accessToken]);

    return(
        <SafeAreaView style={styles.Screen}>
            <View style={styles.screenprofile}>
                <AntDesign name="user" size={100} color="blue" />
            </View>

            <TextInput
                placeholder="Nombre"
                style={styles.input}
                 onChangeText={(text) => setName(text)}
                value={name}
                returnKeyType="next"
                onSubmitEditing={() => emailInputRef.current?.focus()}
            />

            <TextInput
                ref={emailInputRef}
                placeholder="Correo electrónico"
                style={styles.input}
                onChangeText={(text) => setEmail(text)}
                value={email}
                autoCapitalize="none"
                returnKeyType="next"
                onBlur={validateEmail}
            />
            {emailError ? <Text style={styles.errorText}>{emailError}</Text> : null}

            <LoadingButton onPress={async () =>setModalVisible(true)} title="Actualizar Usuario" disabled={!name.trim() || !isValidEmail(email)} />

            <ConfirmationModal
                isVisible={modalVisible}
                message="¿Desea guardar estos cambios?"
                onConfirm={Register}
                onCancel={handleCancelChanges}
            />

        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    Screen: {
      flex: 1 ,
      backgroundColor: '#ffffff',//color 
      alignItems: 'center',//alinea al centro
      justifyContent: 'center',//lo centra aun mas
    },
    titulo:{
        fontSize: 80,//cambia el tamano de la letra
        color: 'green',
        fontWeight: 'bold'
    },
    subtitle:{
        fontSize: 20,
        color: 'blue',
        textDecorationLine: 'underline',
    },
    position_subtitle:{
        backgroundColor:'orange',
        marginTop:10,
        marginEnd:240,
    },
    textImput:{
        borderWidth:1,
        borderColor:'blue',
        paddingStart:30,
        width: '80%',
        height:50,
        padding: 10,
        marginTop:20,//separacion entre las columnas
        borderRadius: 50,
        backgroundColor: 'white'
    },
    avatar:{
        width:100,
        height:100,
        borderRadius:50,
        marginTop:0,
    },
    username:{
        fontSize: 15,
        textAlign:'center',
    },
    boton:{
        //flex:1,
        justifyContent: 'flex-start',
        padding:10,
        borderRadius:30,
        backgroundColor:'#00fff7',
        alignItems:'center'
    },
    screenprofile:{
        backgroundColor:'orange',
        alignItems:'center',
        borderRadius:50,
    },
    container2:{
        flexDirection: 'row', // Alineación horizontal
        justifyContent: 'space-between', // Espacio entre los componentes
        alignItems: 'center', // Centrar verticalmente
    },
    input: {
        borderWidth: 1,
        borderColor: 'blue',
        paddingStart: 30,
        width: '80%',
        height: 50,
        padding: 10,
        marginTop: 20,
        borderRadius: 50,
        backgroundColor: 'white',
      },
      errorText: {
        color: 'red',
        marginTop: 5,
      },

   
  });
export default Profile;
