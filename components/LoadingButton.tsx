// components/LoadingButton.tsx
import React, { useState } from 'react';
import { View, TouchableOpacity, ActivityIndicator, Text, StyleSheet } from 'react-native';

interface LoadingButtonProps {
  onPress: () => Promise<void>;
  title: string;
  disabled?: boolean;
}

const LoadingButton: React.FC<LoadingButtonProps> = ({ onPress, title, disabled = false }) => {
  const [loading, setLoading] = useState(false);

  const handlePress = async () => {
    if (loading || disabled) {
      return;
    }

    setLoading(true);

    try {
      await onPress();
    } catch (error) {
      // Maneja el error según tus necesidades
    } finally {
      setLoading(false);
    }
  };

  return (
    <TouchableOpacity
      style={[styles.button, disabled && styles.disabledButton]}
      onPress={handlePress}
      disabled={disabled}
    >
      <View style={styles.buttonContent}>
        {loading ? (
          <ActivityIndicator size="small" color="white" />
        ) : (
          <Text style={[styles.buttonText, disabled && styles.disabledButtonText]}>{title}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 20,
    borderRadius: 50,
    width: '70%',
  },
  disabledButton: {
    backgroundColor: 'lightgray', // Cambia el color del botón cuando está deshabilitado
    shadowColor: 'black', // Añade un efecto de sombra
    shadowOpacity: 0.3,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 2,
  },
  buttonContent: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 50,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  disabledButtonText: {
    color: 'gray', // Cambia el color del texto cuando está deshabilitado
  },
});

export default LoadingButton;
