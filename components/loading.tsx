import React, { ReactNode } from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";

interface LoadingProps {
  loading: boolean;
  children: ReactNode; 
}

const Loading: React.FC<LoadingProps> = ({ loading, children }) => {
  if (loading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="black" />
      </View>
    );
  } else {
    return <>{children}</>; // Use fragment para evitar envolver children en un div innecesario
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loading;
