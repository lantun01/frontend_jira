import { create } from 'zustand';
import { devtools, persist, createJSONStorage } from 'zustand/middleware';
import AsyncStorage from '@react-native-async-storage/async-storage';

export type UseUserStoreT = {
  accessToken?: string;
  setAccessToken: (accessToken: string) => void;
  removeAccessToken: () => void;
  //email
  email?: string;
  setemail: (email: string) => void;
  removeemail: () => void;
  //id
  id?: number;
  setId: (id: number) => void;
  removeId: () => void;
};

export const UserStore = create<UseUserStoreT>()(
  devtools(
    persist(
      (set, get) => ({
        accessToken: undefined,
        setAccessToken: (accessToken: string) => set(() => ({ accessToken })),
        removeAccessToken: () => set(() => ({ accessToken: undefined })),
      
        email: undefined,
        setemail: (email: string) => set(() => ({ email })),
        removeemail: () => set(() => ({ email: undefined })),
       
        id: undefined,
        setId: (id: number) => set(() => ({ id })),
        removeId: () => set(() => ({ id: undefined })),
      }),
    
      {
        name: 'user-storage',
        storage: createJSONStorage(() => AsyncStorage),
      }
    
    )
  )
);