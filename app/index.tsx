import React from "react";
import { StyleSheet } from "react-native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "../screens/auth/Login";
import RegisterScreen from "../screens/auth/register";
import Home from "./inicio";
import Inicio2 from "./inicio2";
import Inicio3 from "./inicio3";
import MainUser from "../screens/user/Main_user";
import MainTeam from "../screens/team/Main_team";
import MainProyect from "../screens/Proyect/Main_Proyect";
import MainTask from "../screens/Task/MainTask";


import ResetPasswordChangeScreen from "../screens/auth/ResetPasswordChange";
import ResetPasswordCodeScreen from "../screens/auth/ResetPasswordCode";
import ResetPasswordEmailScreen from "../screens/auth/ResetPasswordEmail";

const Stack = createNativeStackNavigator();


const Start = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="inicio" component={Inicio2} />
      <Stack.Screen name="login" component={Login} />
      <Stack.Screen name="register" component={RegisterScreen} />

      <Stack.Screen name="mainValid" component={Inicio3} />

      <Stack.Screen
        name="main_user"
        component={MainUser}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="main_team"
        component={MainTeam}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="main_proyect"
        component={MainProyect}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="main_task"
        component={MainTask}
        options={{ headerShown: false }}
      />
      
      <Stack.Screen
        name="resetPasswordEmail"
        component={ResetPasswordEmailScreen}
      />
      <Stack.Screen
        name="resetPasswordCode"
        component={ResetPasswordCodeScreen}
      />
      <Stack.Screen
        name="resetPasswordChange"
        component={ResetPasswordChangeScreen}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange", //color
    alignItems: "center", //alinea al centro
    justifyContent: "center", //lo centra aun mas
  },
  title: {
    fontSize: 50, //cambia el tamano de la letra
    color: "black",
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "blue",
    paddingStart: 30,
    width: "80%",
    height: 50,
    padding: 10,
    marginTop: 20, //separacion entre las columnas
    borderRadius: 50,
    backgroundColor: "white",
  },
  button: {
    backgroundColor: "blue",
    marginTop: 20,
    padding: 10,
    borderRadius: 50,
    width: "70%",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    textAlign: "center",
  },
});

export default Start;
