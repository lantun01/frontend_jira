import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../types/rootsStackParamList";
import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList>;

interface HomeProps {
  navigation: HomeScreenNavigationProp;
}

const Home: React.FC<HomeProps> = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>menu principal</Text>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("inicio")}
      >
        <Text style={styles.buttonText}>comenzar</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 50,
    color: "black",
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "blue",
    marginTop: 20,
    padding: 10,
    borderRadius: 50,
    width: "70%",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    textAlign: "center",
  },
});

export default Home;
