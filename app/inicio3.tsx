import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity, StyleSheet ,Alert} from 'react-native';
import { UserStore } from '../store/UserStore';
import { interfaceJWT } from '../interfaces/JWT';
import jwtDecode from 'jwt-decode';
import {
  useNavigation,
  NavigationProp,
  RouteProp,
} from "@react-navigation/native";
import { RootStackParamList } from '../types/rootsStackParamList';
import { StackNavigationProp } from '@react-navigation/stack';

//type MainValidScreenRouteProp = RouteProp<RootStackParamList, "mainValid">;

type MainValidScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  "mainValid"
>;

interface HomeProps {
  navigation: MainValidScreenNavigationProp;
}

const Inicio3 : React.FC<HomeProps> = ({ navigation }) => {

  const { accessToken } = UserStore();
    const {removeAccessToken,removeId,removeemail}=UserStore();
    const {email,setemail} =UserStore();
    const {id,setId} =UserStore();

    useEffect(() => {
        if (accessToken === undefined) {
        } else {
          const decoded: interfaceJWT = jwtDecode(accessToken);
          setemail(decoded.email)//guardo email
          setId(decoded.id)//guardo id
        }
      }, [accessToken]);
    
      const borrar = async () =>{
        removeAccessToken;
        removeId();
        removeemail();
        navigation.navigate('Home')
      }
  
  return (
    <View style={styles.container}>

      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('main_user')}>
        <Text style={styles.buttonText}>Metodos User</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('main_team')}>
        <Text style={styles.buttonText}>Metodos Team</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('main_proyect')}>
        <Text style={styles.buttonText}>Metodos Proyect</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('main_task')}>
        <Text style={styles.buttonText}>Metodos Task</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.button2} onPress={borrar}>
        <Text style={styles.buttonText}>Cerrar seccion</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1 ,
    backgroundColor: 'orange',//color 
    alignItems: 'center',//alinea al centro
    justifyContent: 'center',//lo centra aun mas
  },
  title: {
    fontSize: 50,//cambia el tamano de la letra
    color: 'black',
    fontWeight: 'bold',
    
  },
  input: {
    borderWidth:1,
    borderColor:'blue',
    paddingStart:30,
    width: '80%',
    height:50,
    padding: 10,
    marginTop:20,//separacion entre las columnas
    borderRadius: 50,
    backgroundColor: 'white'
  },
  button: {
    backgroundColor: 'blue',
    marginTop:20,
    padding: 10,
    borderRadius: 50,
    width: '70%',
  },
  button2: {
    backgroundColor: 'red',
    marginTop:20,
    padding: 10,
    borderRadius: 50,
    width: '70%',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
});

export default Inicio3;