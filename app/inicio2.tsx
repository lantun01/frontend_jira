import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";

interface Inicio2Props {
  navigation: {
    navigate: (screen: "register" | "login") => void;
  };
}

const Inicio2: React.FC<Inicio2Props> = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("register")}
      >
        <Text style={styles.buttonText}>Registrar</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("login")}
      >
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "orange",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 50,
    color: "black",
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "blue",
    paddingStart: 30,
    width: "80%",
    height: 50,
    padding: 10,
    marginTop: 20,
    borderRadius: 50,
    backgroundColor: "white",
  },
  button: {
    backgroundColor: "blue",
    marginTop: 20,
    padding: 10,
    borderRadius: 50,
    width: "70%",
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    textAlign: "center",
  },
});

export default Inicio2;
