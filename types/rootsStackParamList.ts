export type RootStackParamList = {
  main_user: undefined;
  main_team: undefined;
  main_proyect: undefined;
  main_task: undefined;
  Home: undefined;
  mainValid: undefined;
  login: undefined;
  registro: undefined;
  resetPasswordEmail: undefined;
  resetPasswordCode: { email: string };
  resetPasswordChange: { email: string };
};